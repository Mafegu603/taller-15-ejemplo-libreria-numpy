import numpy as np

#Una empresa de transportes gestiona una flota de 60 camiones de tres modelos diferentes. Los mayores transportan 
#una media diaria de 15000 kg. y recorren diariamente una media de 400 kilómetros. Los medianos transportan diariamente
#una media de 10000 kilogramos y recorren 300 kilómetros. Los pequeños transportan diariamente 5000 kilogramos y 
# recorren 100 km. de media. Diariamente los camiones de la empresa transportan un total de 475 toneladas y recorren
# 12500 km. entre todos. ¿Cuántos camiones gestiona la empresa de cada modelo?

#El sistema de ecuaciones para este problema es:
#x+y+z=60
#15000x+10000y+5000z=47500
#400x+300y+100z=12500

a=np.array([[1,1,1],[15000,10000,5000],[400,300,100]])
b=np.array([60,475000,12500])

r=np.linalg.solve(a,b)
print(r)
